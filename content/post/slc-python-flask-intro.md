---
title: "SLC Python -- Flask Intro"
date: 2019-05-01T22:56:42-06:00
draft: false
---

Terrel Shumway presented an introduction to Flask at SLC Python.

[click here](https://www.loom.com/share/518aba8e8fc64a5fb89268a475160652)
to watch the recorded video.
