---
title: "Welcome"
date: 2019-04-19T11:19:53-06:00
draft: false
---

## What We're About

Calling all Pythonistas in the Northern Utah/Southern Idaho area. 
This is a group for those interested in software development using the Python programming language. 
We will meet monthly in Logan to discuss aspects and capabilities of the language, as well as explore mechanisms for implementing these features in day-to-day code. 
We'll explore why Python is a great choice for web infrastructures, application infrastructures, as well as scientific data analysis. 
Avid and aspiring developers alike are welcome to join.

{{< figure src="/images/pyutno-logo.jpeg" title="Python Utah North Logo" >}}
